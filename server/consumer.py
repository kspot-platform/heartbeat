import json
from datetime import datetime
import threading
import requests

from pymongo import MongoClient 

from kafka import KafkaConsumer

def notify_heartbeat(hearbeat_dict):
    # data = {"text": "<br/><b>DSID:</b>{0}<br/><b>Heartbeat:</b>{1}<br/><b>TIMESTAMP:</b><br/>{2}><br/><b>TYPE:</b><br/>{3}".format(hearbeat_dict['dsid'], hearbeat_dict['heartbeat'], hearbeat_dict['timestamp'], hearbeat_dict['type'])}
    data = {"text": "DSID:{0}\n\nHeartbeat:{1}\n\nTIMESTAMP:{2}\n\nTYPE:{3}".format(hearbeat_dict['dsid'], hearbeat_dict['heartbeat'], hearbeat_dict['timestamp'], hearbeat_dict['type'])}
    teams_url = 'https://outlook.office.com/webhook/2a83fb08-9875-4f6c-bbff-9887a6cb656c@281de859-c72a-4b58-8ef3-430d00dd5f43/IncomingWebhook/3c0c310c1ba64245ae6e12372c122e27/9a0d15b2-55bd-4edc-a3ea-2fee2866797d'
    requests.post(teams_url, data=json.dumps(data))
    slack_url = 'https://hooks.slack.com/services/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'
    requests.post(slack_url, data=json.dumps(data))

def create_mongoclient():
    client = MongoClient("mongodb://localhost:27017/")
    mydatabase = client['kafka_poc']
    mycollection = mydatabase['numtest_table']
    return client, mydatabase, mycollection


def create_consumer():
    consumer = KafkaConsumer('numtest',bootstrap_servers = ['localhost:9092'],
    value_deserializer=lambda m: json.loads(m.decode('utf-8')))
    for message in consumer:
        # print(message.value)
        client, mydatabase, mycollection = create_mongoclient()
        timestamp = datetime.strptime(message[6]['timestamp'], '%Y-%m-%d %H:%M:%S.%f')
        message[6]['timestamp'] = timestamp
        result = mycollection.insert_one(message.value)
        print(result.inserted_id)
        client.close()
        teams_message_thread = threading.Thread(target=notify_heartbeat,args=[message.value])
        teams_message_thread.start()
        

if __name__ == '__main__':
    create_consumer()