import time
import requests
import datetime

def get_command_output(command):
    import subprocess
    out = subprocess.Popen(command, stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    stdout,stderr = out.communicate()
    return stdout.decode('utf-8', errors='replace').replace('\n','<br/>').replace('\t','&emsp;')


def produce_data():
    i = 1
    host = get_command_output(['hostname'])
    # mac = get_command_output(['ipconfig','/all','|', 'findstr', '"Physical Address"'])
    # print(mac)
    print(host)
    while i>0:
        dt = datetime.datetime.now()
        # date = str(dt.strftime("%Y"))+str(dt.strftime("-%m"))+str(dt.strftime("-%d"))
        # time = 
        data_to_send = {
            "type": host,
            "dsid": 'host2',
            "heartbeat": {
                "webServer": {
                "isAlive": True
                },
                "visionService": {
                "isAlive": True
                },
                "cameraStreams": {
                "isAlive": True
                }
            },
            "timestamp": str(dt)
        }
        # x = requests.post('http://127.0.0.1:8080/produce/',json={"hostname":host,"data":str(i),"datetime":str(dt)})
        x = requests.post('http://127.0.0.1:8080/produce/',json=data_to_send)
        i += 1
        print(x.status_code)
        time.sleep(5)

if __name__ == '__main__':
    produce_data()