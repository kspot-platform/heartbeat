from kafka import KafkaProducer
import time
import flask
import json
from flask import request
app = flask.Flask(__name__)
app.config["DEBUG"] = True

producer = KafkaProducer(bootstrap_servers=['localhost:9092'],value_serializer=lambda v: json.dumps(v).encode('utf-8'))

@app.route('/')
def testing():
    return 'Just testing'
# while True:
@app.route('/produce/',methods=['POST'])
def produceData():
    post_data = request.json
    # hostname = post_data['hostname']
    # data = post_data['data']
    # datetime = post_data['datetime']
    # producer.send('numtest', {'hostname': hostname,'data':data,'datetime':datetime})
    producer.send('numtest', post_data)
    return ''
    
if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8080)