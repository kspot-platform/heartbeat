import time
import flask
from flask import jsonify
import json
import pymongo
from pymongo import MongoClient 

from flask import request
app = flask.Flask(__name__)
app.config["DEBUG"] = True

def create_mongoclient():
    client = MongoClient("mongodb://localhost:27017/")
    mydatabase = client['kafka_poc']
    mycollection = mydatabase['numtest_table']
    return client, mydatabase, mycollection


@app.route('/heartbeat/',methods=['GET'])
def get_heartbeats():
    main_list = []
    client, mydatabase, mycollection = create_mongoclient()
    if('start' in request.args and 'limit' in request.args):
        start = int(request.args['start'])
        limit = int(request.args['limit'])
        result = mycollection.aggregate([
            {
                "$group":{
                    "_id":"$dsid",
                    "timestamp":{
                        "$max":'$timestamp'
                    },
                    "heartbeat":{
                        "$first":"$heartbeat"
                    },
                    "type":{
                        "$first":"$type"
                    }
                }
            },
            { 
                "$addFields": { "dsid": "$_id" }
            },
            {
                "$project": { "_id": 0 }
            },
            {
                "$skip":start
            },
            {
                "$limit":limit
            }
        ])

    else:
        result = mycollection.aggregate([
            {
                "$group":{
                    "_id":"$dsid",
                    "timestamp":{
                        "$max":'$timestamp'
                    },
                    "heartbeat":{
                        "$first":"$heartbeat"
                    },
                    "type":{
                        "$first":"$type"
                    }
                }
            },
            { 
                "$addFields": { "dsid": "$_id" }
            },
            {
                "$project": { "_id": 0 }
            }
        ])

    for heartbeat in result:
        main_list.append(heartbeat)
    # for controller in mycollection.distinct('dsid'):
    #     for heartbeat in mycollection.find({"dsid":controller},{ "_id": 0}).sort([("timestamp",-1)]):
    #         # print(heartbeat)
    #         main_list.append(heartbeat)
    #         break
    client.close()
    return jsonify(main_list)

@app.route('/heartbeat/<dsid>',methods=['GET'])
def get_specific_heartbeats(dsid):
    specific_list = []
    client, mydatabase, mycollection = create_mongoclient()

    if('start' in request.args and 'limit' in request.args):
        start = int(request.args['start'])
        limit = int(request.args['limit'])
        result = mycollection.aggregate([
            {
                "$project":{
                    "_id":0
                }
            },
            {
                "$match":{
                    "dsid":dsid
                }
            },
            {
                "$sort":{
                    "timestamp":-1
                }
            },
            {
                "$skip":start
            },
            {
                "$limit":limit
            }
        ])

    else:
        result = mycollection.aggregate([
            {
                "$project":{
                    "_id":0
                }
            },
            {
                "$match":{
                    "dsid":dsid
                }
            },
            {
                "$sort":{
                    "timestamp":-1
                }
            }
        ])

    for heartbeat in result:
        specific_list.append(heartbeat)
    # for heartbeat in mycollection.find({"dsid":dsid},{ "_id": 0}).sort([("timestamp",-1)]):
    #     # print(heartbeat)
    #     specific_list.append(heartbeat)
    client.close()
    return jsonify(specific_list)



if __name__ == '__main__':
  app.run(host='0.0.0.0', port=8083)